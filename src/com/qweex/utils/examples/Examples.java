package com.qweex.utils.examples;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.qweex.utils.*;

import java.security.Key;
import java.util.ArrayList;

public class Examples extends Activity implements DirectoryChooserDialog.OnDirectoryChosen, MultiFileChooserDialog.OnFilesChosen
{
    static ArrayListWithMaximum<Integer> arrayListWithMaximum = new ArrayListWithMaximum<Integer>();
    static int lastNum = 1;
    static {
        arrayListWithMaximum.setMaximumCapacity(5);
        for(; lastNum<5; lastNum++)
            arrayListWithMaximum.add(lastNum);
    }

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        //******************** Directory Chooser Dialog ********************//
        // (also see the function 'onChosenDir')
        findViewById(R.id.directory_chooser).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DirectoryChooserDialog d = new DirectoryChooserDialog(Examples.this, Examples.this);
                d.setNewFolderEnabled(false);
                d.chooseDirectory();
            }
        });

        //******************** Path Preference ********************//
        findViewById(R.id.path_preference).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Examples.this, PathPreferenceExample.class));
            }
        });

        //******************** File Picker Dialog ********************//
        findViewById(R.id.file_picker).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new MultiFileChooserDialog(Examples.this, Examples.this);
            }
        });

        //******************** Image Text Button ********************//
        findViewById(R.id.img_txt_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(((ImgTxtButton)view).getOrientation()==ImgTxtButton.HORIZONTAL)
                    ((ImgTxtButton)view).setOrientation(ImgTxtButton.VERTICAL);
                else
                    ((ImgTxtButton)view).setOrientation(ImgTxtButton.HORIZONTAL);
            }
        });

        //******************** Crypt ********************//
            findViewById(R.id.crypt).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                    String data = "You have no chance to survive make your time";
                    String password = "butts";
                    Key key = Crypt.getKeyFromPassword(password);
                    String encrypted = Crypt.encrypt(data, key);
                    String decrypted = Crypt.decrypt(encrypted, key);

                    new AlertDialog.Builder(Examples.this)
                            .setMessage((
                                    "data: \n" + data + "\n\n" +
                                    "pass: \n" + password + "\n\n" +
                                    "encrypted: \n" + encrypted + "\n\n" +
                                    "decrypted: \n" + decrypted
                            )).show();

                    } catch(Exception e) {
                        Toast.makeText(Examples.this, e.toString(), Toast.LENGTH_LONG);
                    }
                }
            });


        //******************** Number Picker ********************//
        com.qweex.utils.NumberPicker picker = (com.qweex.utils.NumberPicker) findViewById(R.id.number_picker);
        picker.setPrefix("I have ");
        picker.setSuffix(" bananas");


        //******************** XBMC Style List Menu ********************//
        findViewById(R.id.xbmc_style_list_menu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Examples.this, XBMCExample.class));
            }
        });


        //******************** Array With Maximum ********************//
        findViewById(R.id.array_list_with_maximum).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                arrayListWithMaximum.add(++lastNum);
                ((TextView) view).setText("ArrayListWithMaximum: " + arrayListWithMaximum.toString());
            }
        });
        ((TextView)findViewById(R.id.array_list_with_maximum)).setText("ArrayListWithMaximum: " + arrayListWithMaximum.toString());


        //******************** Is Tablet Device? ********************//
                ((TextView) findViewById(R.id.is_tablet_device)).setText("IsTabletDevice: " + QweexUtils.isTabletDevice(this));
    }

    @Override
    public void onDirectoryChosen(String chosenDir) {
        Toast.makeText(this, chosenDir, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFilesChosen(ArrayList<String> files) {
        if(files!=null)
            Toast.makeText(this, files.toString(), Toast.LENGTH_SHORT).show();
    }
}
